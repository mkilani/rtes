      SUBROUTINE LOCQ(METHOD,EMPLOC,EMPGEN,NOBS,NLD,DELTA,MATLQ,LQ,
     +                                                  DEBUG)
      IMPLICIT NONE
C
C     Author: Moez Kilani
C     Date: March 2012
C     Contact: moez [dot] kilani [at] univ-lille3.fr
C
C
C     INPUT
C        METHOD:  the method used to compute the coefficient
C                 matrix. There are four methods accepted :
C                  SLQ, CILQ, RLQ, FLQ
C        EMPLOC:  local employment data, an vector of NOBS
C                 elements
C        EMPGEN:  global employment data (at nation level, in general)
C        NOBS:    number of sectors considered.
C     OUTPUT
C        MATLQ:   the coefficient matrix
C
      INTEGER NOBS,NLD,DEBUG
      REAL*8 EMPLOC(*),EMPGEN(*),MATLQ(NLD,*)
      CHARACTER*5 METHOD,METHODS(5)
      REAL*8 TOTLOC,TOTGEN,LQ(*),DELTA,LAMBDA
      CHARACTER*10 FMT

      INTEGER I,J

      DATA METHODS /'SLQ','CILQ','RLQ','FLQ','NAT'/

      TOTLOC=0.D0
      TOTGEN=0.D0
      DO I=1,NOBS
         TOTLOC=TOTLOC+EMPLOC(I)
         TOTGEN=TOTGEN+EMPGEN(I)
      ENDDO
      DO I=1,NOBS
         LQ(I)=(EMPLOC(I)/TOTLOC)/(EMPGEN(I)/TOTGEN)
      ENDDO

      IF(METHOD.EQ.METHODS(1)) THEN
         GOTO 1   !  method SLQ
      ELSEIF(METHOD.EQ.METHODS(2)) THEN
         GOTO 2   ! method CILQ
      ELSEIF(METHOD.EQ.METHODS(3)) THEN
         GOTO 3   ! method RLQ
      ELSEIF(METHOD.EQ.METHODS(4)) THEN
         GOTO 4   ! method FLQ
      ELSEIF(METHOD.EQ.METHODS(5)) THEN
         GOTO 5   ! method NAT
      ENDIF

C     If none of the methods is matched then print
C     an error message here and go to the end
      PRINT*, 'ERROR: Method ', METHOD, ' is not implemented'
      GOTO 100

   1  CONTINUE
C     METHOD 1: SLQ
      DO I=1,NOBS
         DO J=1,NOBS
            MATLQ(I,J)=MIN(1.D0,LQ(I))
         ENDDO
      ENDDO
      GOTO 100


   2  CONTINUE
C     METHOD 2: CILQ
      DO I=1,NOBS
         DO J=1,NOBS
            MATLQ(I,J)=MIN(1.D0,LQ(I)/LQ(J))
         ENDDO
      ENDDO
      GOTO 100


   3  CONTINUE
C     METHOD 3: RLQ 
      DO I=1,NOBS
         DO J=1,NOBS
            MATLQ(I,J)=MIN(1.D0,LQ(I)/(LOG(1.D0+LQ(J))/LOG(2.D0)))
         ENDDO
      ENDDO
      GOTO 100

   4  CONTINUE
C     METHOD 4: FLQ
      LAMBDA=(LOG(1.D0+TOTLOC/TOTGEN)/LOG(2.D0))**DELTA
      DO I=1,NOBS
         DO J=1,NOBS
            MATLQ(I,J)=MIN(1.D0,LAMBDA*LQ(I)/LQ(J))
         ENDDO
      ENDDO
      DO I=1,NOBS
         MATLQ(I,I)=MIN(1.D0,LAMBDA*LQ(I))
      ENDDO
      GOTO 100 

   5  CONTINUE
C     METHOD 5: NAT
      DO I=1,NOBS
         DO J=1,NOBS
            MATLQ(I,J)=1.0D0
         ENDDO
      ENDDO
      GOTO 100 

 100  CONTINUE 

      IF(DEBUG.EQ.1)THEN
        WRITE(FMT,'("("I0,"F9.3)")') NOBS
        PRINT*,'LOCATION QUOTIENT MATRIX'
        DO I=1,NOBS
          WRITE(*,FMT),(MATLQ(I,J),J=1,NOBS)
        ENDDO
      ENDIF

      RETURN
      ENDSUBROUTINE
