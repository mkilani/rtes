      SUBROUTINE REGIONAL(NLD,NOBS,COEF,MATLQ,LEONTIEF,MULTIPLIERS,
     +                                                     DEBUG)
C     ------------------- 
C     TAKES A MATRIX "A" AND PRODUCES A LEONTIEF MATRIX
C     AND COMPUTES MULTIPLIERS
      IMPLICIT NONE
      INTEGER NOBS,NLD,DEBUG
      REAL*8 COEF(NLD,*),MATLQ(NLD,*),LEONTIEF(NLD,*)
      REAL*8 MULTIPLIERS(NOBS)
      CHARACTER*10 FMT

C     LOCAL DATA
      INTEGER I,J
      REAL*8 MATID(NOBS,NOBS), MATTMP(NOBS,NOBS)

      DO I=1,NOBS
        DO J=1,NOBS
           MATID(I,J)=0.D0
        ENDDO
      ENDDO
      DO I=1,NOBS
         MATID(I,I)=1.D0
      ENDDO 

      DO 10 I=1,NOBS
      DO 10 J=1,NOBS
         COEF(I,J)=COEF(I,J)*MATLQ(I,J)
         LEONTIEF(I,J)=MATID(I,J)-COEF(I,J)

  10  CONTINUE

      IF(DEBUG.EQ.1)THEN
        WRITE(FMT,'("("I0,"F9.3)")') NOBS
        PRINT*,'MATRIX A'
        DO I=1,NOBS
          WRITE(*,FMT),(COEF(I,J),J=1,NOBS)
        ENDDO

        PRINT*,'MATRIX I-A'
        DO I=1,NOBS
          WRITE(*,FMT),(LEONTIEF(I,J),J=1,NOBS)
        ENDDO
      ENDIF

      CALL MATINV(LEONTIEF,NLD,NOBS)

      IF(DEBUG.EQ.1)THEN
        WRITE(FMT,'("("I0,"F9.3)")') NOBS
        PRINT*,'LEONTIEF MATRIX (I-A)^(-1)'
        DO I=1,NOBS
          WRITE(*,FMT),(LEONTIEF(I,J),J=1,NOBS)
        ENDDO
      ENDIF

      MULTIPLIERS=0.D0
      DO J=1,NOBS
         DO I=1,NOBS
            MULTIPLIERS(J)=MULTIPLIERS(J)+LEONTIEF(I,J)
         ENDDO
      ENDDO


      RETURN
      ENDSUBROUTINE




