TES tools -- version 0.1
Marsh 31th, 2012

Author: Moez Kilani

TES tools is a set of fortran routines
that performs input-output analysis.
It is basically intended to perform
regional analysis.

Installation
============

There are mainly two steps.

1. Linux (debian/ubuntu): in terminal mode
unpack the content of the rtes.tar file

The tar file content:

- maintes.f : main program
- input.f : read data file
- lq.f : computes location quotients and the matrix
used to adjust the national one.
- regional.f : estimates local coeficient matrix
based on the national matrix and location quotient method
- matcoef.f : computes the coefient matrix
- matinv.f : inverts a matrix (relies on lapack subroutines)
- quick_sort2.f : a subroutine for sorting output
written by Leonard J. Moss.

2. Type "make"

Important: you should have:
- lapack, and
- gfortran
installed to proceed.

Compilation on other plateforms has not been
tried but should not be complicate. 
For example, the command:

	gfortran -o rtes.exe *.f -llapack

should produce the excutable on DOS/WINDOWS terminal.

Running
=======

1. prepare an input file. See the provided example
"example.9" for the structure of such file.
Since the interpreter is rudimentary and does
not perform serious syntax check, the input file 
should be correct, or unexpected output is produced.

2. Use the command
       ./rtes input-file
where "input-file" is the data file.

Remarks
=======

1. tes uses lapack subroutine to inverse the I-A
matrix. 

2. The sorting of the output relies on the
quick sort subroutine written by Leonard J. Moss
and given in the file "quick_sort2.f".




