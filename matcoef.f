      SUBROUTINE MATCOEF(TES,PROD,COEF,NOBS,NLD,DEBUG)
* ***************************************************************
*                                                               *
* Summary:                                                      *
* --------                                                      *
* Transforms an initial input-output table                      *
* to a table of technical coefficients                          *
*                                                               *
* Variables:                                                    *
* ----------                                                    *
* TES (input)   : (NOBSxNOBS)-matrix of the input-output table  *
* PROD (input)  : (1xNOBS)-vector of production values          *
* COEF (output) : the table of technical coefficients           *
* NOBS (input)  : the number of obesrvations in the problem     *
* NLD (input)   : the leading dimension of the tables           *
* DEBUG (input) : a binary variable that indicates whether      *
*                 intermediate computations are shown or not    *
*                                                               *
* Author : Moez Kilani                                          *
* Date : April 2012; revised June 2012                          *
*                                                               *
*****************************************************************
      IMPLICIT NONE
      INTEGER NOBS,NLD,DEBUG
      REAL*8 TES(NLD,*),PROD(*),COEF(NLD,*) 
C     LOCAL VARIABLES
      INTEGER I,J
      REAL*8 TOT
      CHARACTER*10 FMT

      DO I=1,NOBS
         DO J=1,NOBS
            COEF(I,J)=TES(I,J)/PROD(J)
         ENDDO
      ENDDO


      IF(DEBUG.EQ.1)THEN
        WRITE(FMT,'("("I0,"F9.3)")') NOBS
        PRINT*,'TECHNICAL COEFICIENTS'
        DO I=1,NOBS
          WRITE(*,FMT),(COEF(I,J),J=1,NOBS)
        ENDDO
      ENDIF

      RETURN
      ENDSUBROUTINE

