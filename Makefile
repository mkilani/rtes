# Project : input-output analysis tools
#
# Moez Kilani, Marsh 29th, 2012
#
CC = gfortran
EXEC = rtes
LDFLAGS = -llapack -lslatec 

all: $(EXEC) 

rtes: maintes.o input.o lq.o matcoef.o matinv.o regional.o quick_sort2.o
	$(CC) -o $@ $^ $(LDFLAGS) 

%.o: %.f
	$(CC) -o $@ -c $<

# Utility targets
#
.PHONY: clean 

clean:
	rm -f *.o 

