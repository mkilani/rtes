      SUBROUTINE MATINV(A,NLD,NB)
C     ======================================================
C     COMPUTES THE INVERSE OF A MATRIX A
C
C     PARAMETERS:
C     -----------
C       A (INPUT/OUTPUT): ON INPUT THE MATRIX TO INVERSE
C                         AND ON OUTPUT THE RESULT
C       NB (INPUT): THE DIMENSION OF MATRXI A IS NB x NB
C       NLD: LEADING DIMENSION OF A
C
C     NOTE : USES LAPACK SUBROUTINES "DGETRF" AND "DGETRI"      
C     ======================================================
      INTEGER IPIV,INFO,LDA
      REAL*8 WORK
      INTEGER I,J,NB
      REAL*8 A(NLD,*)
      PARAMETER(IWORK=1000)
      DIMENSION WORK(IWORK)
      DIMENSION IPIV(NB)
      LDA=NB
C     ****************
C     LU decomposition
C     DGETRF : a lapack subroutine
C     ****************
      CALL DGETRF(NB,NB,A,NLD,IPIV,INFO)
      IF (INFO.NE.0) THEN
          PRINT *, "Echec dans la décomposition LU"
      ENDIF
C     *****************************************
C     Invert from LU decomposition
C     DGETRI: a lapack subroutine
C     *****************************************
      CALL DGETRI(NB,A,NLD,IPIV,WORK,IWORK,INFO)
      IF (INFO.NE.0) THEN
          PRINT *, "Echec dans l'inversion de la matrice"
      ENDIF
      RETURN
      ENDSUBROUTINE

